package net.therap.mealmanager.utility;

import java.sql.*;

/**
 * @author arafat
 * @since 11/13/16
 */
public class DbConnector {

    private static Connection connection;
    private static String DATABASE_NAME = "meal_management";

    private static final String URL = "jdbc:mysql://localhost:3306/" + DATABASE_NAME +
            "?autoReconnect=true&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    public static Connection createConnection() throws ClassNotFoundException {
        try {
            if(connection==null) {
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
            }
        } catch (SQLException se) {
            System.out.println("Unable to connect to database");
        }

        return connection;
    }

    public static Connection getConnection(){
        return connection;
    }

    public static void closeConnection() throws SQLException {
        if(connection!=null){
            close(connection);
        }
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(PreparedStatement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
