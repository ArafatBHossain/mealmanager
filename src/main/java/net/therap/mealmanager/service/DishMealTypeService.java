package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.DishMealTypeDao;
import net.therap.mealmanager.domain.DishMealType;
import java.sql.SQLException;

/**
 * @author arafat
 * @since 11/14/16
 */
public class DishMealTypeService {

    private DishMealTypeDao dishMealTypeDao;

    public void addDishMealType(int dishId, int mealTypeId) throws
            SQLException, ClassNotFoundException {
        dishMealTypeDao = new DishMealTypeDao();
        dishMealTypeDao.save(new DishMealType(dishId, mealTypeId));
    }
}
