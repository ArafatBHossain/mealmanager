package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.DishDao;
import net.therap.mealmanager.domain.Dish;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
public class DishService {

    private DishDao dishDao;

    public List<Dish> getListOfDishesMyMealType(int mealTypeId)
            throws SQLException, ClassNotFoundException {
        dishDao = new DishDao();
        return dishDao.getListById(mealTypeId);
    }

    public int addDish(Dish dish) throws SQLException, ClassNotFoundException {
        dishDao = new DishDao();
        return dishDao.save(dish);
    }

}
