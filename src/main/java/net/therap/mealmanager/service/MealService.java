package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.MealDao;
import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.WeekDay;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/14/16
 */
public class MealService {

    private MealDao mealDao;

    public List<Dish> getMealListByWeekDayAndMealtype(int weekDayId, int mealTypeId)
            throws SQLException,
            ClassNotFoundException {
        mealDao = new MealDao();
        return mealDao.getMealListOfMeal(weekDayId, mealTypeId);
    }

    public void addMeal(Meal meal) throws SQLException, ClassNotFoundException {
        mealDao = new MealDao();
        mealDao.save(meal);

    }

    public List<WeekDay> getListOfWeekDays() {
        return mealDao.getList();
    }

    public String getWeekDayNameById(int weekDayId) throws SQLException, ClassNotFoundException {
        mealDao = new MealDao();
        return mealDao.getWeekDayName(weekDayId);
    }

    public void deleteDishFromMenu(int dishId) throws SQLException, ClassNotFoundException {
        mealDao = new MealDao();
        mealDao.deleteByDishId(dishId);
    }
}
