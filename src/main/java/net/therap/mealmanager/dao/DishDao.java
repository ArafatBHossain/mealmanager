package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.utility.DbConnector;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
public class DishDao {

    private PreparedStatement preparedStatement;

    private String TABLE_DISH = "dish";
    private String TABLE_DISH_MEAL_TYPE = "dish_meal_type";

    private String DISH_NAME = "dish_name";
    private String DISH_ID = "dish_id";
    private String MEAL_TYPE_ID = "meal_type_id";

    private String getDishIdStatement = "SELECT DISH_ID FROM "+
            TABLE_DISH+" WHERE "+ DISH_NAME+"=?";
    private String addDishStatement = "INSERT INTO "+ TABLE_DISH+" VALUES(?,?)";
    private String getDishListStatement = "SELECT * FROM "+ TABLE_DISH+" where "+
            DISH_ID+" IN(SELECT "+DISH_ID+" FROM "+TABLE_DISH_MEAL_TYPE+
            " WHERE "+MEAL_TYPE_ID+"=?)";



    public int getDishId(String dishName) {

        ResultSet resultSetGetDishList = null;
        int dishId = 0;

        try {
            preparedStatement = DbConnector.getConnection().prepareStatement(getDishIdStatement);
            preparedStatement.setString(1, dishName);
            resultSetGetDishList = preparedStatement.executeQuery();

            while (resultSetGetDishList.next()) {
                dishId = resultSetGetDishList.getInt(DISH_ID);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(resultSetGetDishList);
            DbConnector.close(preparedStatement);
        }

        return dishId;
    }

    public int save(Dish dish) {
        int last_inserted_id = 0;
        ResultSet rs = null;
        try {
            preparedStatement = DbConnector.getConnection().prepareStatement(addDishStatement,
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, dish.getDishId());
            preparedStatement.setString(2, dish.getDishName());
            preparedStatement.execute();
            rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                last_inserted_id = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(preparedStatement);
            DbConnector.close(rs);
        }

        return last_inserted_id;
    }

    public List<Dish> getListById(int mealTypeId) {
        List<Dish> dishList = new ArrayList<>();

        ResultSet resultSetGetDishList = null;

        try {
            preparedStatement = DbConnector.getConnection().prepareStatement(getDishListStatement);
            preparedStatement.setInt(1, mealTypeId);
            resultSetGetDishList = preparedStatement.executeQuery();

            while (resultSetGetDishList.next()) {
                Dish dish = new Dish();

                dish.setDishId(resultSetGetDishList.getInt(DISH_ID));
                dish.setDishName(resultSetGetDishList.getString(DISH_NAME));

                dishList.add(dish);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(resultSetGetDishList);
            DbConnector.close(preparedStatement);
        }

        return dishList;
    }

}
