package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.utility.DbConnector;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author arafat
 * @since 11/14/16
 */
public class MealDao {

    private String TABLE_DISH = "dish";
    private String TABLE_WDAY_MTYPE_DISH = "wday_mtype_dish";
    private String TABLE_WEEKDAYS = "weekdays";

    private String DISH_ID = "dish_id";
    private String DISH_NAME = "dish_name";
    private String MEAL_TYPE_ID = "meal_type_id";
    private String W_ID = "w_id";
    private String DAY = "day";

    private String getListOfMealByDayAndTypeStatement = "select "+DISH_ID+", "+
            DISH_NAME+" from "+
            TABLE_DISH+" where " +
            DISH_ID+" in(select "+ DISH_ID+" from " +
            TABLE_WDAY_MTYPE_DISH+ " where "+ W_ID+"=? and "+MEAL_TYPE_ID+"=?)";

    private String saveMealStatement = "insert into "+TABLE_WDAY_MTYPE_DISH+
            "("+MEAL_TYPE_ID+", "+
            DISH_ID+", "+ W_ID+") values(?,?,?)";

    private String getListOfDaysStatement = "select * from "+TABLE_WEEKDAYS;

    private String deleteDishFromMenuStatement = "delete from "+TABLE_WDAY_MTYPE_DISH+
            " where "+DISH_ID+"=?";
    private String getWeekDayNameStatement = "select "+DAY+" from "+TABLE_WEEKDAYS+
            " where "+W_ID+"=?";

    private PreparedStatement preparedStatement;

    private boolean saved;

    public List<Dish> getMealListOfMeal(int weekDayId, int mealTypeId) {
        List<Dish> dishList = new ArrayList<>();

        ResultSet resultSetGetDishList = null;

        try {
            preparedStatement = DbConnector.getConnection().prepareStatement
                    (getListOfMealByDayAndTypeStatement);
            preparedStatement.setInt(1, weekDayId);
            preparedStatement.setInt(2, mealTypeId);

            resultSetGetDishList = preparedStatement.executeQuery();

            while (resultSetGetDishList.next()) {
                Dish dish = new Dish();

                dish.setDishId(resultSetGetDishList.getInt(DISH_ID));
                dish.setDishName(resultSetGetDishList.getString(DISH_NAME));

                dishList.add(dish);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(resultSetGetDishList);
            DbConnector.close(preparedStatement);
        }

        return dishList;
    }

    public boolean save(Meal meal) {
        saved = false;
        try {
            preparedStatement = DbConnector.getConnection().prepareStatement(saveMealStatement);
            preparedStatement.setInt(1, meal.getMealTypeId());
            preparedStatement.setInt(2, meal.getDishId());
            preparedStatement.setInt(3, meal.getWeekDayId());
            saved = preparedStatement.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(preparedStatement);
        }
        return saved;
    }

    public List<WeekDay> getList() {
        List<WeekDay> dayList = new ArrayList<>();

        ResultSet resultSetGetDayList = null;

        try {
            preparedStatement = DbConnector.getConnection().prepareStatement(getListOfDaysStatement);

            resultSetGetDayList = preparedStatement.executeQuery();

            while (resultSetGetDayList.next()) {
                WeekDay weekDay = new WeekDay();

                weekDay.setWeekDayId(resultSetGetDayList.getInt(W_ID));
                weekDay.setDayName(resultSetGetDayList.getString(DAY));

                dayList.add(weekDay);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(resultSetGetDayList);
            DbConnector.close(preparedStatement);
        }

        return dayList;
    }

    public String getWeekDayName(int weekDayId) {
        ResultSet resultSetGetDayList = null;
        String dayName = "";
        try {
            preparedStatement = DbConnector.getConnection().
                    prepareStatement(getWeekDayNameStatement);
            preparedStatement.setInt(1, weekDayId);
            resultSetGetDayList = preparedStatement.executeQuery();

            while (resultSetGetDayList.next()) {
                dayName = resultSetGetDayList.getString(DAY);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(resultSetGetDayList);
            DbConnector.close(preparedStatement);
        }

        return dayName;
    }

    public boolean deleteByDishId(int dishId) {
        saved = false;

        try {
            preparedStatement = DbConnector.getConnection().
                    prepareStatement(deleteDishFromMenuStatement);
            preparedStatement.setInt(1, dishId);
            saved = preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(preparedStatement);
        }

        return saved;
    }
}
