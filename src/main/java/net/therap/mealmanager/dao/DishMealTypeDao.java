package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.DishMealType;
import net.therap.mealmanager.utility.DbConnector;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
public class DishMealTypeDao extends GenericDao<DishMealType> {

    private String TABLE_DISH_MEAL_TYPE = "dish_meal_type";

    private String DISH_ID = "dish_id";
    private String MEAL_TYPE_ID = "meal_type_id";

    private String addDishMealTypeStatement = "INSERT INTO "+
            TABLE_DISH_MEAL_TYPE+"("+DISH_ID+", "+ MEAL_TYPE_ID+") VALUES(?,?)";

    private PreparedStatement preparedStatement;

    @Override
    public void save(DishMealType dishMealType) throws SQLException, ClassNotFoundException {
        try {
            preparedStatement = DbConnector.getConnection().
                    prepareStatement(addDishMealTypeStatement);
            preparedStatement.setInt(1, dishMealType.getDishId());
            preparedStatement.setInt(2, dishMealType.getMealTypeId());
            preparedStatement.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(preparedStatement);
        }
    }

    @Override
    public List<DishMealType> getList() {
        return null;
    }

    @Override
    public void update(DishMealType dishMealType) {
        return;
    }

    @Override
    public void delete(int dishId) {
        return ;
    }
}
