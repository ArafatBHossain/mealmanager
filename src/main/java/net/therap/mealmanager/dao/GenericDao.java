package net.therap.mealmanager.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
public abstract class GenericDao<T> {

    public abstract void save(T t) throws SQLException, ClassNotFoundException;

    public abstract List<T> getList();

    public abstract void update(T t);

    public abstract void delete(int id);

}
