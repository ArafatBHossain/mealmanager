package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.utility.DbConnector;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
public class MealTypeDao extends GenericDao<MealType> {

    private PreparedStatement preparedStatement;

    private String TABLE_MEAL_TYPE = "meal_type";
    private String MEAL_TYPE_NAME = "meal_type_name";
    private String MEAL_TYPE_ID = "meal_type_id";

    private String addMealTypeStatement = "INSERT INTO "+ TABLE_MEAL_TYPE+
            "("+MEAL_TYPE_NAME+") VALUES(?)";
    private String getMealTypeListStatement = "SELECT * FROM "+ TABLE_MEAL_TYPE;


    @Override
    public void save(MealType mealType) {
        try {
            preparedStatement = DbConnector.getConnection().prepareStatement(addMealTypeStatement);
            preparedStatement.setString(1, mealType.getMealTypeName());
            preparedStatement.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(preparedStatement);
        }
    }

    @Override
    public List<MealType> getList() {
        List<MealType> mealTypeList = new ArrayList<>();

        ResultSet resultSetGetMealList = null;

        try {
            preparedStatement = DbConnector.getConnection().prepareStatement(getMealTypeListStatement);
            resultSetGetMealList = preparedStatement.executeQuery();

            while (resultSetGetMealList.next()) {
                MealType mealType = new MealType();

                mealType.setMealTypeId(resultSetGetMealList.getInt(MEAL_TYPE_ID));
                mealType.setMealTypeName(resultSetGetMealList.getString(MEAL_TYPE_NAME));

                mealTypeList.add(mealType);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbConnector.close(resultSetGetMealList);
            DbConnector.close(preparedStatement);
        }

        return mealTypeList;
    }

    @Override
    public void update(MealType mealType) {

    }

    @Override
    public void delete(int id) {

    }
}
