package net.therap.mealmanager.domain;

/**
 * @author arafat
 * @since 11/13/16
 */
public class MealType {

    private int mealTypeId;
    private String mealTypeName;

    public MealType(){

    }

    public MealType(String mealTypeName){
        this.setMealTypeName(mealTypeName);
    }

    public int getMealTypeId() {
        return mealTypeId;
    }

    public void setMealTypeId(int mealTypeId) {
        this.mealTypeId = mealTypeId;
    }

    public String getMealTypeName() {
        return mealTypeName;
    }

    public void setMealTypeName(String mealTypeName) {
        this.mealTypeName = mealTypeName;
    }
}
