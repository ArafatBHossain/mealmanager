package net.therap.mealmanager.domain;

/**
 * @author arafat
 * @since 11/14/16
 */
public class WeekDay {

    private int weekDayId;
    private String dayName;

    public int getWeekDayId() {
        return weekDayId;
    }

    public void setWeekDayId(int weekDayId) {
        this.weekDayId = weekDayId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }
}
