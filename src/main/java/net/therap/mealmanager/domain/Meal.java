package net.therap.mealmanager.domain;

/**
 * @author arafat
 * @since 11/14/16
 */
public class Meal {

    private int weekDayId;
    private int dishId;
    private int mealTypeId;

    public Meal(){

    }

    public Meal(int weekDayId, int dishId, int mealTypeId){
        this.setWeekDayId(weekDayId);
        this.setDishId(dishId);
        this.setMealTypeId(mealTypeId);
    }

    public int getWeekDayId() {
        return weekDayId;
    }

    public void setWeekDayId(int weekDayId) {
        this.weekDayId = weekDayId;
    }

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public int getMealTypeId() {
        return mealTypeId;
    }

    public void setMealTypeId(int mealTypeId) {
        this.mealTypeId = mealTypeId;
    }
}
