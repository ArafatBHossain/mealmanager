package net.therap.mealmanager.domain;

/**
 * @author arafat
 * @since 11/13/16
 */
public class DishMealType {

    private int dishId;
    private int mealTypeId;

    public DishMealType(int dishId, int mealTypeId){
        this.setDishId(dishId);
        this.setMealTypeId(mealTypeId);
    }

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public int getMealTypeId() {
        return mealTypeId;
    }

    public void setMealTypeId(int mealTypeId) {
        this.mealTypeId = mealTypeId;
    }
}
