package net.therap.mealmanager.domain;

/**
 * @author arafat
 * @since 11/13/16
 */
public class Dish {

    private int dishId;
    private String dishName;

    public Dish(){

    }

    public Dish(int dishId, String dishName){
        this.setDishId(dishId);
        this.setDishName(dishName);
    }

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }
}
