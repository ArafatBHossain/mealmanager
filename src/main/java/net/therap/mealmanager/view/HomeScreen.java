package net.therap.mealmanager.view;

import net.therap.mealmanager.utility.DbConnector;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class HomeScreen {

    public static void setHomeScreen() throws SQLException, ClassNotFoundException {

        String underlineVariableForView = "-----------------------------------------------" +
                "-----------------\n";

        System.out.printf("%s%40s%s", underlineVariableForView, "Weekly Meal Planner\n",
                underlineVariableForView);
        System.out.printf("%20s%s", "[1] ", "View Meal For Each Day\n");
        System.out.printf("%20s%s", "[2] ", "Manage Type of Meal\n");
        System.out.printf("%20s%s", "[3] ", "Manage Dishes\n");
        System.out.printf("%20s%s", "[4] ", "Prepare Menu\n");
        System.out.printf("%20s%s", "[0] ", "Exit\n\n");

        System.out.print("Press 1/2/3/4 to proceed: ");

        Scanner scanner = new Scanner(System.in);

        int menuScreen = scanner.nextInt();

        switch (menuScreen) {
            case 1:
                new ViewAndEditMealScreen().showMealForEachDay();
                break;

            case 2:
                new ManageMealTypeScreen().manageMealType();
                break;

            case 3:
                new ManageDishesScreen().manageDishes();
                break;

            case 4:
                new PrepareMenuScreen().manageMenuForEachDay();
                break;

            case 0:
                DbConnector.closeConnection();
                break;
        }
    }
}
