package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.DishMealTypeService;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ManageDishesScreen {

    private DishService dishService;
    private MealTypeService mealTypeService;
    private DishMealTypeService dishMealTypeService;

    private List<MealType> mealTypeList;

    String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";

    public void manageDishes() throws SQLException, ClassNotFoundException {

        mealTypeService = new MealTypeService();
        dishService = new DishService();
        dishMealTypeService = new DishMealTypeService();

        mealTypeList = mealTypeService.getAllMealType();
        int mealTypeId = 0;
        Scanner scannerDishNameInput = new Scanner(System.in);
        String dishName;

        System.out.printf("%s%40s%s", underlineVariableForView, "Manage Dishes\n",
                underlineVariableForView);

        for (MealType mealType : mealTypeList) {
            System.out.printf("%22s",mealType.getMealTypeName()+"\n");
        }

        System.out.print("Select Meal Type [Press B to go back]:");

        String inputTypeOfMeal = new Scanner(System.in).next();

        for (MealType mealType : mealTypeList) {
            if (mealType.getMealTypeName().equals(inputTypeOfMeal)) {
                mealTypeId = mealType.getMealTypeId();
                break;
            }
        }

        if (inputTypeOfMeal.equals("B")) {
            HomeScreen.setHomeScreen();

        } else {
            while (true) {
                Dish dish = new Dish();
                System.out.print("Enter dish name [Type Q to stop adding and quit]: ");
                dishName = scannerDishNameInput.nextLine();
                if (dishName.equals("Q")) {
                    HomeScreen.setHomeScreen();
                } else {
                    dish.setDishName(dishName);
                    System.out.println(dish.getDishName());
                    int dishId = dishService.addDish(dish);
                    dishMealTypeService.addDishMealType(dishId, mealTypeId);
                }

            }
        }
    }
}
