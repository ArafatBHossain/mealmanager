package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class PrepareMenuScreen {

    private MealTypeService mealTypeService;
    private MealService mealService;
    private DishService dishService;

    private List<MealType> mealTypeList;
    private List<Dish> listOfDishByMealType;

    private int mealTypeId;
    private int dishId;

    public void manageMenuForEachDay() throws SQLException, ClassNotFoundException {
        mealTypeService = new MealTypeService();
        dishService = new DishService();
        mealService = new MealService();

        mealTypeList = mealTypeService.getAllMealType();

        String underlineVariableForView = "-----------------------------------------------" +
                "-----------------\n";

        System.out.printf("%s%40s%s", underlineVariableForView,"Prepare Menu\n",
                underlineVariableForView);
        System.out.printf("%20s%s%20s%s%20s%s%20s%s%20s%s%20s%s","[1] ", "Sunday\n", "[2] ", "Monday\n",
                "[3] ", "Tuesday\n", "[4] ", "Wednesday\n", "[5] ", "Thursday\n", "[0] ", " Go Back\n");
        System.out.print("Select Day: ");
        Scanner dayInputScanner = new Scanner(System.in);
        int weekDayId = dayInputScanner.nextInt();

        if (weekDayId == 0) {
            HomeScreen.setHomeScreen();
        }

        for (MealType mealType : mealTypeList) {
            System.out.printf("%22s",mealType.getMealTypeName()+"\n");
        }

        System.out.print("\n\nSelect Meal Box: ");

        String inputTypeOfMeal = new Scanner(System.in).next();
        for (MealType mealType : mealTypeList) {
            if (mealType.getMealTypeName().equals(inputTypeOfMeal)) {
                mealTypeId = mealType.getMealTypeId();
                break;
            }
        }

        listOfDishByMealType = dishService.getListOfDishesMyMealType(mealTypeId);

        for (Dish dish : listOfDishByMealType) {
            System.out.printf("%20s%15s","["+String.valueOf(dish.getDishId())+"]",dish.getDishName()+"\n");
        }

        Scanner inputDishScanner = new Scanner(System.in);

        while (true) {
            System.out.print("Enter id to add dishes to menu[Press 0 to stop]: ");
            dishId = inputDishScanner.nextInt();

            if (dishId == 0) {
                HomeScreen.setHomeScreen();
            } else {
                mealService.addMeal(new Meal(weekDayId, dishId, mealTypeId));
            }

        }
    }
}
