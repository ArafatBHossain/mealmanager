package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ManageMealTypeScreen {

    private MealTypeService mealTypeService;
    private List<MealType> listOfMealType;

    public void manageMealType() throws SQLException, ClassNotFoundException {
        mealTypeService = new MealTypeService();

        String underlineVariableForView = "-----------------------------------------------" +
                "-----------------\n";

        System.out.printf("%s%40s%s", underlineVariableForView, "Manage Meal type\n",
                underlineVariableForView);
        System.out.printf("%20s%s", "[A] ", "Add Meal Type\n");
        System.out.printf("%20s%s", "[V] ", "View All Meal Types\n");
        System.out.printf("%20s%s", "[B] ", "Go To Previous Menu\n");

        Scanner sc = new Scanner(System.in);
        String input = sc.next();

        switch (input) {
            case "A":
                System.out.print("Please enter type of meal[B to go to previous menu]: ");
                Scanner scannerInputMealType = new Scanner(System.in);
                String mealTypeName = scannerInputMealType.next();
                if(!mealTypeName.equals("B")){
                   mealTypeService.addMealType(new MealType(mealTypeName));
                }else {
                    manageMealType();
                }
                System.out.println();
                manageMealType();

            case "V":
                listOfMealType = mealTypeService.getAllMealType();
                System.out.printf("%s%40s%s", underlineVariableForView, "Meal types\n",
                        underlineVariableForView);
                for (MealType mealType : listOfMealType) {
                    System.out.printf("%40s", mealType.getMealTypeName() + "\n");
                }
                System.out.println();
                manageMealType();

            case "B":
                HomeScreen.setHomeScreen();
                System.out.println();
                manageMealType();

        }
    }
}
