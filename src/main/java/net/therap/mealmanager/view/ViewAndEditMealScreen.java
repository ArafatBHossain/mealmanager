package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ViewAndEditMealScreen {

    private MealTypeService mealTypeService;
    private MealService mealService;
    private DishService dishService;

    private List<MealType> mealTypeList;
    private List<Dish> listOfDishByDayAndType;

    private int mealTypeId = 0;
    private String weekDay;

    private String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";

    public void showMealForEachDay() throws SQLException, ClassNotFoundException {
        mealTypeService = new MealTypeService();
        mealService = new MealService();

        mealTypeList = mealTypeService.getAllMealType();

        System.out.printf("%s%40s%s", underlineVariableForView, "View and Edit Panel\n",
                underlineVariableForView);
        System.out.printf("%20s%s%20s%s%20s%s%20s%s%20s%s%20s%s", "[1] ", "Sunday\n", "[2] ", "Monday\n",
                "[3] ", "Tuesday\n", "[4] ", "Wednesday\n", "[5] ", "Thursday\n", "[0] ", " Go Back\n");
        System.out.print("Select Day: ");

        Scanner dayInputScanner = new Scanner(System.in);

        int weekDayId = dayInputScanner.nextInt();

        if (weekDayId == 0) {
            HomeScreen.setHomeScreen();

        } else {
            weekDay = mealService.getWeekDayNameById(weekDayId);

            for (MealType mealType : mealTypeList) {
                System.out.printf("%22s", mealType.getMealTypeName() + "\n");
            }

            System.out.print("\nSelect Meal Box: ");
            String inputTypeOfMeal = new Scanner(System.in).next();

            for (MealType mealType : mealTypeList) {
                if (mealType.getMealTypeName().equals(inputTypeOfMeal)) {
                    mealTypeId = mealType.getMealTypeId();
                    break;
                }
            }

            listOfDishByDayAndType = mealService.getMealListByWeekDayAndMealtype(weekDayId, mealTypeId);
            showMenu(listOfDishByDayAndType, weekDay);

            System.out.print("[Y]Go back [N]Continue Viewing [A] Add dishes to menu [D] Delete dishes from menu : ");
            String decisionInput = new Scanner(System.in).next();

            switch (decisionInput) {
                case "Y":
                    showMealForEachDay();
                    break;

                case "N":
                    showMealForEachDay();
                    break;

                case "D":
                    System.out.print("Select ID to delete: ");
                    int dishIdToDelete = new Scanner(System.in).nextInt();

                    mealService.deleteDishFromMenu(dishIdToDelete);
                    listOfDishByDayAndType = mealService.getMealListByWeekDayAndMealtype(weekDayId, mealTypeId);
                    showMenu(listOfDishByDayAndType, weekDay);
                    showMealForEachDay();

                case "A":
                    addDishToExistingMenu(weekDayId, mealTypeId);
                    break;
            }
        }
    }



    public void showMenu(List<Dish> dishList, String weekDay) {
        System.out.printf("%s%40s%s", underlineVariableForView,
                "Menu for " + weekDay+"\n", underlineVariableForView);
        for (Dish dish : dishList) {
            System.out.printf("%20s%15s","["+String.valueOf(dish.getDishId())+"]",dish.getDishName()+"\n");
        }
    }

    public void addDishToExistingMenu(int weekDayId, int mealTypeId) throws SQLException, ClassNotFoundException {
        dishService = new DishService();
        List<Dish> listOfDishByMealType = dishService.getListOfDishesMyMealType(mealTypeId);
        List<Dish> listOfDishByDayAndType;

        for (Dish dish : listOfDishByMealType) {
            System.out.printf("%20s%s","["+String.valueOf(dish.getDishId())+"] ",dish.getDishName()+"\n");
        }

        while (true) {
            System.out.print("Enter ID to add dishes to menu (Press 0 to Stop Adding)");

            int dishIdInput = new Scanner(System.in).nextInt();

            if (dishIdInput == 0) {
                listOfDishByDayAndType = mealService.getMealListByWeekDayAndMealtype(weekDayId, mealTypeId);
                showMenu(listOfDishByDayAndType, mealService.getWeekDayNameById(weekDayId));
                showMealForEachDay();

            } else {
                mealService.addMeal(new Meal(weekDayId, dishIdInput, mealTypeId));
            }

        }
    }
}
